<?php declare(strict_types=1);


namespace App\Basket\Model\Event;


use Prooph\EventSourcing\AggregateChanged;

final class ShoppingSessionStarted extends AggregateChanged
{
    public function basketId() : BasketId
    {
        return BasketId::fromString($this->aggregateId());
    }

    public function shoppingSession() : ShoppingSession
    {
        return ShoppingSession::fromString($this->payload()['shopping_session']);
    }
}