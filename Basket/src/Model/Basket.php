<?php declare(strict_types=1);


namespace App\Basket\Model;


use App\Basket\Model\Event\ShoppingSessionStarted;
use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;

final class Basket extends AggregateRoot
{
    public static function startShoppingSession(
        ShoppingSession $shoppingSession,
        BasketId $basketId
    ) : self {
        $self = new self();

        $self->recordThat(ShoppingSessionStarted::occur($basketId->toString(), [
            'shopping_session' => $shoppingSession->toString()
        ]));

        return $self;
    }

    protected function aggregateId(): string
    {
        // TODO: Implement aggregateId() method.
    }

    protected function apply(AggregateChanged $event): void
    {
        // TODO: Implement apply() method.
    }

}